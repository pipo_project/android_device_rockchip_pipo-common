#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

RKBOOTIMG := $(HOST_OUT_EXECUTABLES)/rkbootimg
RKCRC := $(LOCAL_PATH)/rkcrc

INSTALLED_BOOTIMAGE_TARGET := $(PRODUCT_OUT)/boot.img
INSTALLED_RECOVERYIMAGE_TARGET := $(PRODUCT_OUT)/recovery.img
INSTALLED_KERNELIMAGE_TARGET :=  $(PRODUCT_OUT)/kernel.img

ifeq ($(PACK_BOOT_WITH_KERNEL),true)
$(INSTALLED_BOOTIMAGE_TARGET): $(PRODUCT_OUT)/kernel $(INSTALLED_RAMDISK_TARGET) $(BOARD_MKBOOTIMG_ARGS) $(RKBOOTIMG)
	$(call pretty,"Boot image: $@")
	$(hide) $(RKBOOTIMG) --kernel $(PRODUCT_OUT)/kernel --ramdisk $(INSTALLED_RAMDISK_TARGET) $(BOARD_MKBOOTIMG_ARGS) --output $@
else
$(INSTALLED_BOOTIMAGE_TARGET): $(INSTALLED_RAMDISK_TARGET) $(RKCRC)
	$(call pretty,"Boot image: $@")
	$(hide) $(RKCRC) -k $(INSTALLED_RAMDISK_TARGET) $@
endif

ifeq ($(PACK_RECOVERY_WITH_KERNEL),true)
$(INSTALLED_RECOVERYIMAGE_TARGET): $(PRODUCT_OUT)/kernel $(recovery_ramdisk) $(BOARD_MKBOOTIMG_ARGS) $(RKBOOTIMG)
	@echo ----- Making RK recovery image ------
	$(hide) $(RKBOOTIMG) --kernel $(PRODUCT_OUT)/kernel --ramdisk $(recovery_ramdisk) $(BOARD_MKBOOTIMG_ARGS) --output $@
	@echo ----- Made RK recovery image -------- $@
else
$(INSTALLED_RECOVERYIMAGE_TARGET): $(recovery_ramdisk) $(RKCRC)
	@echo ----- Making RK recovery image ------
	$(hide) $(RKCRC) -k $(recovery_ramdisk) $@
	@echo ----- Made RK recovery image -------- $@
endif

ifeq ($(STANDALONE_KERNEL),true)
$(INSTALLED_KERNELIMAGE_TARGET): $(PRODUCT_OUT)/kernel $(RKCRC)
	@echo ----- Making RK kernel image ------
	$(hide) $(RKCRC) -k $(PRODUCT_OUT)/kernel $@
	@echo ----- Made RK kernel image -------- $@
endif